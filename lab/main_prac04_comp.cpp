/*---------------------------------------------------------*/
/* ----------------   Pr�ctica 4 --------------------------*/
/*-----------------    2020-1   ---------------------------*/
/*------------- Rodrigo Francisco Pablo ---------------*/
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "include/shader_m.h"

#include <iostream>

void resize(GLFWwindow* window, int width, int height);
void my_input(GLFWwindow *window);

// settings
// Window size
int SCR_WIDTH = 3800;
int SCR_HEIGHT = 7600;

GLFWmonitor *monitors;
GLuint VBO, VAO, EBO;

void myData(void);
void display(Shader);
void getResolution(void);

//For Keyboard
float	movX = 0.0f,
		movY = 0.0f,
		movZ = -5.0f,
		rotX = 0.0f,
		rotY = 0.0f,
		rotZ = 0.0f;


void getResolution()
{
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	SCR_WIDTH = mode->width;
	SCR_HEIGHT = (mode->height) - 80;
}

void myData()
{	
		GLfloat vertices[] = {
		//Position				//Color
		-0.5f, -0.5f, 0.5f,		1.0f, 0.0f, 0.0f,	//V0 - Frontal
		0.5f, -0.5f, 0.5f,		1.0f, 0.0f, 0.0f,	//V1
		0.5f, 0.5f, 0.5f,		1.0f, 0.0f, 0.0f,	//V5
		-0.5f, 0.5f, 0.5f,		1.0f, 0.0f, 0.0f,	//V4

		0.5f, -0.5f, -0.5f,		1.0f, 1.0f, 0.0f,	//V2 - Trasera
		-0.5f, -0.5f, -0.5f,	1.0f, 1.0f, 0.0f,	//V3
		-0.5f, 0.5f, -0.5f,		1.0f, 1.0f, 0.0f,	//V7
		0.5f, 0.5f, -0.5f,		1.0f, 1.0f, 0.0f,	//V6

		-0.5f, 0.5f, 0.5f,		0.0f, 0.0f, 1.0f,	//V4 - Izq
		-0.5f, 0.5f, -0.5f,		0.0f, 0.0f, 1.0f,	//V7
		-0.5f, -0.5f, -0.5f,	0.0f, 0.0f, 1.0f,	//V3
		-0.5f, -0.5f, 0.5f,		0.0f, 0.0f, 1.0f,	//V0

		0.5f, 0.5f, 0.5f,		0.0f, 1.0f, 0.0f,	//V5 - Der
		0.5f, -0.5f, 0.5f,		0.0f, 1.0f, 0.0f,	//V1
		0.5f, -0.5f, -0.5f,		0.0f, 1.0f, 0.0f,	//V2
		0.5f, 0.5f, -0.5f,		0.0f, 1.0f, 0.0f,	//V6

		-0.5f, 0.5f, 0.5f,		1.0f, 0.0f, 1.0f,	//V4 - Sup
		0.5f, 0.5f, 0.5f,		1.0f, 0.0f, 1.0f,	//V5
		0.5f, 0.5f, -0.5f,		1.0f, 0.0f, 1.0f,	//V6
		-0.5f, 0.5f, -0.5f,		1.0f, 0.0f, 1.0f,	//V7

		-0.5f, -0.5f, 0.5f,		1.0f, 1.0f, 1.0f,	//V0 - Inf
		-0.5f, -0.5f, -0.5f,	1.0f, 1.0f, 1.0f,	//V3
		0.5f, -0.5f, -0.5f,		1.0f, 1.0f, 1.0f,	//V2
		0.5f, -0.5f, 0.5f,		1.0f, 1.0f, 1.0f,	//V1
	};

	unsigned int indices[] =	//I am not using index for this session
	{
		0
	};

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// color attribute
	//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	//glEnableVertexAttribArray(1);

	//Para trabajar con indices (Element Buffer Object)
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

}

void display(Shader shader)
{
	//Shader myShader("shaders/shader.vs", "shaders/shader.fs");
	

	shader.use();

	// create transformations and Projection
	glm::mat4 temp = glm::mat4(1.0f);
	glm::mat4 so = glm::translate(glm::mat4(1.0f), glm::vec3(10.0f, 4.0f, -4.0f));
	glm::mat4 temp2 = glm::mat4(1.0f);
	glm::mat4 model = glm::mat4(1.0f);		// initialize Matrix, Use this matrix for individual models
	glm::mat4 view = glm::mat4(1.0f);		//Use this matrix for ALL models
	glm::mat4 projection = glm::mat4(1.0f);	//This matrix is for Projection

	projection = glm::perspective(glm::radians(45.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	//projection = glm::ortho(-5.0f, 5.0f, -3.0f, 3.0f, 0.1f, 10.0f);

	//Use "view" in order to affect all models
	view = glm::translate(view, glm::vec3(movX, movY, movZ));
	view = glm::rotate(view, glm::radians(rotX), glm::vec3(1.0f, 1.0f, 0.0f));
	view = glm::rotate(view, glm::radians(rotY), glm::vec3(0.0f, 1.0f, 0.0f));
	view = glm::rotate(view, glm::radians(rotZ), glm::vec3(0.0f, 0.0f, 1.0f));
	// pass them to the shaders
	shader.setMat4("model", model);
	shader.setMat4("view", view);
	// note: currently we set the projection matrix each frame, but since the projection matrix rarely 
	//changes it's often best practice to set it outside the main loop only once.
	shader.setMat4("projection", projection);


	glBindVertexArray(VAO);
	
	shader.setVec3("aColor", glm::vec3(0.52f, 0.33f, 0.39f));
	model = glm::scale(model, glm::vec3(5.0f, 1.5f, 2.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); //upper jaw part 1

	temp = model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.25f, 0.0f));
	model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.25f, 0.75f));
	model = glm::scale(model, glm::vec3(1.0f, 1.0f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); //upper jaw part 2

	temp = model = glm::translate(temp, glm::vec3(-1.25f, 0.0f, 0.75f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.3f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(0.32f, 0.23f, 0.26f));
	glDrawArrays(GL_QUADS, 0, 24); // left nostrill center

	shader.setVec3("aColor", glm::vec3(0.52f, 0.33f, 0.39f));
	model = glm::translate(temp, glm::vec3(0.0f, 0.5f, 0.0f));
	model = glm::scale(model, glm::vec3(1.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left nostrill top
	
	model = glm::translate(temp, glm::vec3(0.0f, -0.5f, 0.0f));
	model = glm::scale(model, glm::vec3(1.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left nostrill bottom

	model = glm::translate(temp, glm::vec3(-0.5f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left nostrill left

	model = glm::translate(temp, glm::vec3(0.5f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left nostrill right

	model = glm::translate(temp, glm::vec3(-1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 1.0f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left upper jaw part 3

	temp = model = glm::translate(glm::mat4(1.0f), glm::vec3(1.25f, 1.25f, 0.75f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.3f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(0.32f, 0.23f, 0.26f));
	glDrawArrays(GL_QUADS, 0, 24); // right nostrill center

	shader.setVec3("aColor", glm::vec3(0.52f, 0.33f, 0.39f));
	model = glm::translate(temp, glm::vec3(0.0f, 0.5f, 0.0f));
	model = glm::scale(model, glm::vec3(1.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right nostrill top
	
	model = glm::translate(temp, glm::vec3(0.0f, -0.5f, 0.0f));
	model = glm::scale(model, glm::vec3(1.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right nostrill bottom

	model = glm::translate(temp, glm::vec3(-0.5f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right nostrill left

	model = glm::translate(temp, glm::vec3(0.5f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right nostrill right

	model = glm::translate(temp, glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 1.0f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right upper jaw part 4

	//big change
	temp =model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.25f, -0.25f));
	model = glm::scale(model, glm::vec3(5.0f, 1.0f, 1.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // upper jaw part 5

	temp = model = glm::translate(temp, glm::vec3(0.0f, -0.75f, -1.0f));
	model = glm::scale(model, glm::vec3(4.0f, 2.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // upper jaw behind part 6

	shader.setVec3("aColor", glm::vec3(0.09f, 0.09f, 0.09f));
	temp = model = glm::translate(temp, glm::vec3(-0.9f, 1.5f, -0.15f));
	model = glm::scale(model, glm::vec3(0.4f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left eye center

	shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 1.0f));
	model = glm::translate(temp, glm::vec3(-0.4f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.4f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left eye left-part

	model = glm::translate(temp, glm::vec3(0.4f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.4f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left eye right-part

	model = glm::translate(temp, glm::vec3(0.0f, 0.5f, 0.0f));
	model = glm::scale(model, glm::vec3(1.2f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left eye top-part

	shader.setVec3("aColor", glm::vec3(0.09f, 0.09f, 0.09f));
	temp = model = glm::translate(temp, glm::vec3(1.8f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.4f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right eye center

	shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 1.0f));
	model = glm::translate(temp, glm::vec3(-0.4f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.4f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left eye left-part

	model = glm::translate(temp, glm::vec3(0.4f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.4f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left eye right-part

	model = glm::translate(temp, glm::vec3(0.0f, 0.5f, 0.0f));
	model = glm::scale(model, glm::vec3(1.2f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // left eye top-part


	shader.setVec3("aColor", glm::vec3(0.52f, 0.33f, 0.39f));
	temp = model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -2.0f, 0.0f));
	model = glm::scale(model, glm::vec3(5.0f, 1.0f, 2.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // bottom jaw

	shader.setVec3("aColor", glm::vec3(0.7f, 0.3f, 0.37f));
	temp = model = glm::translate(temp, glm::vec3(0.0f, 0.75f, 0.0f));
	model = glm::scale(model, glm::vec3(2.5f, 0.5f, 1.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); //center teeth

	shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 1.0f));
	model = glm::translate(temp, glm::vec3(-1.625f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.75f, 0.5f, 1.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); //left teeth

	model = glm::translate(temp, glm::vec3(1.625f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.75f, 0.5f, 1.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); //right teeth

	shader.setVec3("aColor", glm::vec3(0.43f, 0.27f, 0.31f));
	temp2 = temp = model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, -2.25f)); // Take good care of this ref
	model = glm::scale(model, glm::vec3(6.0f, 4.5f, 1.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); //head

	temp = model = glm::translate(temp, glm::vec3(3.25f, 2.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear part 1

	shader.setVec3("aColor", glm::vec3(0.52f, 0.33f, 0.39f));
	temp = model = glm::translate(temp, glm::vec3(0.625f, -0.125f, 0.125f));
	model = glm::scale(model, glm::vec3(0.75f, 0.25f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear part 2

	model = glm::translate(temp, glm::vec3(-0.25f, 0.375f, 0.0f));
	model = glm::scale(model, glm::vec3(0.25f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear part 3

	model = glm::translate(temp, glm::vec3(0.25f, 0.375f, 0.0f));
	model = glm::scale(model, glm::vec3(0.25f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear part 4

	shader.setVec3("aColor", glm::vec3(0.32f, 0.23f, 0.26f));
	temp = model = glm::translate(temp, glm::vec3(0.0f, 0.375f, 0.0f));
	model = glm::scale(model, glm::vec3(0.25f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear part 5

	shader.setVec3("aColor", glm::vec3(0.52f, 0.33f, 0.39f));
	model = glm::translate(temp, glm::vec3(0.0f, 0.375f, 0.0f));
	model = glm::scale(model, glm::vec3(0.75f, 0.25f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear part 6

	shader.setVec3("aColor", glm::vec3(0.43f, 0.27f, 0.31f));
	temp2 = model = glm::translate(temp2, glm::vec3(-3.25f, 2.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 0.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear2 part 1

	shader.setVec3("aColor", glm::vec3(0.52f, 0.33f, 0.39f));
	temp2 = model = glm::translate(temp2, glm::vec3(-0.625f, -0.125f, 0.125f));
	model = glm::scale(model, glm::vec3(0.75f, 0.25f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear2 part 2

	model = glm::translate(temp2, glm::vec3(-0.25f, 0.375f, 0.0f));
	model = glm::scale(model, glm::vec3(0.25f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear2 part 3

	model = glm::translate(temp2, glm::vec3(0.25f, 0.375f, 0.0f));
	model = glm::scale(model, glm::vec3(0.25f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear2 part 4

	shader.setVec3("aColor", glm::vec3(0.32f, 0.23f, 0.26f));
	temp2 = model = glm::translate(temp2, glm::vec3(0.0f, 0.375f, 0.0f));
	model = glm::scale(model, glm::vec3(0.25f, 0.5f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear2 part 5

	shader.setVec3("aColor", glm::vec3(0.52f, 0.33f, 0.39f));
	model = glm::translate(temp2, glm::vec3(0.0f, 0.375f, 0.0f));
	model = glm::scale(model, glm::vec3(0.75f, 0.25f, 0.25f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // right ear2 part 6

	shader.setVec3("aColor", glm::vec3(0.48f, 0.33f, 0.39f));
	temp = model = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, -2.25f)); 
	// we are now in the center of the head, head meas are (6.0f, 4.5f, 1.5f)
	model = glm::translate(temp, glm::vec3(0.0f, -2.75f, 0.0f));
	model = glm::scale(model, glm::vec3(6.0f, 1.0f, 1.5f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // Bottom head

	shader.setVec3("aColor", glm::vec3(0.43f, 0.27f, 0.31f));
	temp = model = glm::translate(temp, glm::vec3(0.0f, 0.0f, -3.75f));
	model = glm::scale(model, glm::vec3(8.0f, 5.0f, 6.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // body

	shader.setVec3("aColor", glm::vec3(0.48f, 0.33f, 0.39f));
	temp = model = glm::translate(temp, glm::vec3(0.0f, -3.0f, 0.0f));
	model = glm::scale(model, glm::vec3(8.0f, 1.0f, 2.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // Bottom body

	model = glm::translate(temp, glm::vec3(0.0f, 0.0f, 2.0f));
	model = glm::scale(model, glm::vec3(4.0f, 1.0f, 2.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // Bottom body 2
	
	model = glm::translate(temp, glm::vec3(0.0f, 0.0f, -2.0f));
	model = glm::scale(model, glm::vec3(4.0f, 1.0f, 2.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // Bottom body 3

	shader.setVec3("aColor", glm::vec3(0.43f, 0.27f, 0.31f));
	model = glm::translate(temp, glm::vec3(3.0f, -1.0f, 2.0f));
	model = glm::scale(model, glm::vec3(2.0f, 3.0f, 2.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // pie1

	model = glm::translate(temp, glm::vec3(-3.0f, -1.0f, 2.0f));
	model = glm::scale(model, glm::vec3(2.0f, 3.0f, 2.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // pie2

	model = glm::translate(temp, glm::vec3(3.0f, -1.0f, -2.0f));
	model = glm::scale(model, glm::vec3(2.0f, 3.0f, 2.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // pie3

	model = glm::translate(temp, glm::vec3(-3.0f, -1.0f, -2.0f));
	model = glm::scale(model, glm::vec3(2.0f, 3.0f, 2.0f));
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); // pie4


	model = glm::scale(so, glm::vec3(4.0f, 4.5f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 0.0f));
	glDrawArrays(GL_QUADS, 0, 24); //Pecho

	temp = model = glm::translate(so, glm::vec3(0.0f, 2.5f, .0f));
	model = glm::scale(model, glm::vec3(0.5f, 0.5f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(0.0f, 1.0f, 0.0f));
	glDrawArrays(GL_QUADS, 0, 24); //CUello

	model = glm::translate(temp, glm::vec3(0.0f, 1.625f, 0.0f));
	model = glm::scale(model, glm::vec3(2.0f, 2.75f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //cabeza

	temp = model = glm::translate(so, glm::vec3(0.0f, -3.125f, 0.0f));
	model = glm::scale(model, glm::vec3(4.0f, 1.75f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //cadera

	model = glm::translate(temp, glm::vec3(-1.25f, -2.5f, 0.0f));
	model = glm::scale(model, glm::vec3(1.5f, 4.0f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //pierna izq

	temp = model = glm::translate(temp, glm::vec3(1.25f, -2.5f, 0.0f));
	model = glm::scale(model, glm::vec3(1.5f, 4.0f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 1.0f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //pierna der

	
	model = glm::translate(temp, glm::vec3(0.5f, -2.5f, 0.0f));
	model = glm::scale(model, glm::vec3(2.5f, 1.0f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(0.3f, .5f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //zapato der

	model = glm::translate(temp, glm::vec3(-3.0f, -2.5f, 0.0f));
	model = glm::scale(model, glm::vec3(2.5f, 1.0f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(0.3f, .5f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //zapato izq

	temp = model = glm::translate(so, glm::vec3(-3.0f, 1.75f, 0.0f));
	model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(0.3f, .5f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //brazo izq

	model = glm::translate(temp, glm::vec3(-0.5f, -2.25f, 0.0f));
	model = glm::scale(model, glm::vec3(1.0f, 3.5f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 0.9f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //antebrazo izq

	temp = model = glm::translate(so, glm::vec3(3.0f, 1.75f, 0.0f));
	model = glm::scale(model, glm::vec3(2.0f, 1.0f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(0.3f, .5f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //brazo der

	temp = model = glm::translate(temp, glm::vec3(0.5f, -2.25f, 0.0f));
	model = glm::scale(model, glm::vec3(1.0f, 3.5f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 0.9f, 1.0f));
	glDrawArrays(GL_QUADS, 0, 24); //antebrazo der

	temp = model = glm::translate(temp, glm::vec3(0.0f, -2.25f, 0.0f));
	model = glm::scale(model, glm::vec3(1.5f, 1.0f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 0.3f, 0.3f));
	glDrawArrays(GL_QUADS, 0, 24); //espada parte 1

	temp = model = glm::translate(temp, glm::vec3(1.125f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(0.75f, 2.0f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 0.3f, 0.3f));
	glDrawArrays(GL_QUADS, 0, 24); //espada parte 2

	temp = model = glm::translate(temp, glm::vec3(2.375f, 0.0f, 0.0f));
	model = glm::scale(model, glm::vec3(4.0f, 1.0f, 1.0f));
	shader.setMat4("model", model);
	shader.setVec3("aColor", glm::vec3(1.0f, 0.3f, 0.3f));
	glDrawArrays(GL_QUADS, 0, 24); //espada parte 3



	glBindVertexArray(0);

}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    /*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);*/

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
	monitors = glfwGetPrimaryMonitor();
	getResolution();

    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Practica 4", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
	glfwSetWindowPos(window, 0, 30);
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, resize);

	glewInit();


	//Mis funciones
	//Datos a utilizar
	myData();
	glEnable(GL_DEPTH_TEST);

	Shader projectionShader("shaders/shader_projection.vs", "shaders/shader_projection.fs");

    // render loop
    // While the windows is not closed
    while (!glfwWindowShouldClose(window))
    {
        // input
        // -----
        my_input(window);

        // render
        // Backgound color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Mi funci�n de dibujo
		display(projectionShader);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void my_input(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)  //GLFW_RELEASE
        glfwSetWindowShouldClose(window, true);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		movX += 0.08f;
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		movX -= 0.08f;
	if (glfwGetKey(window, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
		movY += 0.08f;
	if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
		movY -= 0.08f;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		movZ -= 0.08f;
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		movZ += 0.08f;


	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		rotY += 0.5f;
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		rotY -= 0.5f;

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		rotX += 0.5f;
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		rotX -= 0.5f;
	if (glfwGetKey(window, GLFW_KEY_HOME) == GLFW_PRESS)
		rotZ += 0.5f;
	if (glfwGetKey(window, GLFW_KEY_END) == GLFW_PRESS)
		rotZ -= 0.5f;

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void resize(GLFWwindow* window, int width, int height)
{
    // Set the Viewport to the size of the created window
    glViewport(0, 0, width, height);
}