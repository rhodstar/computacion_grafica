/*---------------------------------------------------------*/
/* ----------------   Práctica 3 --------------------------*/
/*-----------------    2020-1   ---------------------------*/
/*------------- Rodrigo Francisco Pablo ------------------*/
#include "include/glew.h"
#include "include/glfw3.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "include/shader_m.h"

#include <iostream>

void resize(GLFWwindow* window, int width, int height);
void my_input(GLFWwindow *window);

// settings
// Window size
int SCR_WIDTH = 3800;
int SCR_HEIGHT = 7600;

GLFWmonitor *monitors;
GLuint VBO, VAO, EBO;

void myData(void);
void display(void);
void getResolution(void);

/**
	The following functions help us to draw cubes in an easiest way
*/
void drawLonelyCube(glm::mat4& model,Shader& projectionShader,glm::vec3 v);
void drawNTimesCube(glm::mat4& model,Shader& projectionShader,glm::vec3 v,int times);
void blankSpace(glm::mat4& model,Shader& projectionShader,glm::vec3 v);
void walk(glm::mat4& model,Shader& projectionShader,glm::vec3 coords[],int size);
void drawFigure(glm::mat4& model,Shader& projectionShader,glm::vec3 coords[],int size);
void drawNegativeFigure(glm::mat4& model,Shader& projectionShader,glm::vec3 coords[],int size);
void drawReverseNegativeFigure(glm::mat4& model,Shader& projectionShader,glm::vec3 coords[],int size);

//For Keyboard
float movX = -5.0f;
float movY = 5.0f;
float movZ = -25.0f;

// basic movements
glm::vec3 goRight = glm::vec3(1.0,0.0,0.0);
glm::vec3 goLeft = glm::vec3(-1.0,0.0,0.0);
glm::vec3 goUp = glm::vec3(0.0,1.0,0.0);
glm::vec3 goDown = glm::vec3(0.0,-1.0,0.0);
glm::vec3 goRightUp = glm::vec3(1.0,1.0,0.0);
glm::vec3 goRightDown = glm::vec3(1.0,-1.0,0.0);
glm::vec3 goLeftUp = glm::vec3(-1.0,1.0,0.0);
glm::vec3 goLeftDown = glm::vec3(-1.0,-1.0,0.0);

// Colors for emoji
glm::vec3 yellow = glm::vec3(1.0,1.0,0.0);
glm::vec3 red = glm::vec3(1.0,0.0,0.0);
glm::vec3 orange = glm::vec3(1.0, 0.5, 0.0);
glm::vec3 brown = glm::vec3(0.8, 0.2, 0.0);

/**
	This function draws a cube in a given position
*/
void drawLonelyCube(glm::mat4& model,Shader& projectionShader,glm::vec3 v){
	model = glm::translate(model,v );
	projectionShader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 0, 24); 
}

/**
	This functions draws N cubes in a given direction.
	The arguments times = N
*/
void drawNTimesCube(glm::mat4& model,Shader& projectionShader,glm::vec3 v,int times){
	for (int i = 0; i< times; i++){
		model = glm::translate(model,v );
		projectionShader.setMat4("model", model);
		glDrawArrays(GL_QUADS, 0, 24); 
	}

}

/** 
	This function jumps or gives space in a given position
	DOES NOT DRAW A CUBE
*/
void blankSpace(glm::mat4& model,Shader& projectionShader,glm::vec3 v){
	model = glm::translate(model,v );
	projectionShader.setMat4("model", model);
}

/** 
	This function jumps in a set of positions given, does NOT draw any cubes
*/
void walk(glm::mat4& model,Shader& projectionShader,glm::vec3 coords[],int size){
	for (int i = 0; i < size; i++){
		blankSpace(model,projectionShader,coords[i]);
	}
}

/** 
	This function draws a figure with the positions given in the param "coods",
	besides, we need to specify the size of the set of positions.
*/
void drawFigure(glm::mat4& model,Shader& projectionShader,glm::vec3 coords[],int size){
	for (int i = 0; i < size; i++){
		drawLonelyCube(model,projectionShader,coords[i]);
	}
}

/** 
	This function does the same as draw figure but multiplying by the scalar -1
*/
void drawNegativeFigure(glm::mat4& model,Shader& projectionShader,glm::vec3 coords[],int size){
	for (int i = 0; i < size; i++){
		drawLonelyCube(model,projectionShader,coords[i]*(-1.0f));
	}
}
/** 
	This function does the same as draw figure but multiplying by the scalar -1 and besides, 
	start by the last element of the position set.
*/
void drawReverseNegativeFigure(glm::mat4& model,Shader& projectionShader,glm::vec3 coords[],int size){
	for (int i = size; i >= 0; i--){
		drawLonelyCube(model,projectionShader,coords[i]*(-1.0f));
	}
}

void getResolution()
{
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	SCR_WIDTH = mode->width;
	SCR_HEIGHT = (mode->height) - 80;
}

void myData()
{	
		GLfloat vertices[] = {
		//Position				//Color
		-0.5f, -0.5f, 0.5f,		1.0f, 0.0f, 0.0f,	//V0 - Frontal
		0.5f, -0.5f, 0.5f,		1.0f, 0.0f, 0.0f,	//V1
		0.5f, 0.5f, 0.5f,		1.0f, 0.0f, 0.0f,	//V5
		-0.5f, 0.5f, 0.5f,		1.0f, 0.0f, 0.0f,	//V4

		0.5f, -0.5f, -0.5f,		1.0f, 1.0f, 0.0f,	//V2 - Trasera
		-0.5f, -0.5f, -0.5f,	1.0f, 1.0f, 0.0f,	//V3
		-0.5f, 0.5f, -0.5f,		1.0f, 1.0f, 0.0f,	//V7
		0.5f, 0.5f, -0.5f,		1.0f, 1.0f, 0.0f,	//V6

		-0.5f, 0.5f, 0.5f,		0.0f, 0.0f, 1.0f,	//V4 - Izq
		-0.5f, 0.5f, -0.5f,		0.0f, 0.0f, 1.0f,	//V7
		-0.5f, -0.5f, -0.5f,	0.0f, 0.0f, 1.0f,	//V3
		-0.5f, -0.5f, 0.5f,		0.0f, 0.0f, 1.0f,	//V0

		0.5f, 0.5f, 0.5f,		0.0f, 1.0f, 0.0f,	//V5 - Der
		0.5f, -0.5f, 0.5f,		0.0f, 1.0f, 0.0f,	//V1
		0.5f, -0.5f, -0.5f,		0.0f, 1.0f, 0.0f,	//V2
		0.5f, 0.5f, -0.5f,		0.0f, 1.0f, 0.0f,	//V6

		-0.5f, 0.5f, 0.5f,		1.0f, 0.0f, 1.0f,	//V4 - Sup
		0.5f, 0.5f, 0.5f,		1.0f, 0.0f, 1.0f,	//V5
		0.5f, 0.5f, -0.5f,		1.0f, 0.0f, 1.0f,	//V6
		-0.5f, 0.5f, -0.5f,		1.0f, 0.0f, 1.0f,	//V7

		-0.5f, -0.5f, 0.5f,		1.0f, 1.0f, 1.0f,	//V0 - Inf
		-0.5f, -0.5f, -0.5f,	1.0f, 1.0f, 1.0f,	//V3
		0.5f, -0.5f, -0.5f,		1.0f, 1.0f, 1.0f,	//V2
		0.5f, -0.5f, 0.5f,		1.0f, 1.0f, 1.0f,	//V1
	};

	unsigned int indices[] =	//I am not using index for this session
	{
		0
	};

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	//Para trabajar con indices (Element Buffer Object)
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

}

void display(void)
{
	//Shader myShader("shaders/shader.vs", "shaders/shader.fs");
	Shader projectionShader("shaders/shader_projection.vs", "shaders/shader_projection.fs");

	projectionShader.use(); // Aqui los activo

	// create transformations and Projection
	glm::mat4 model = glm::mat4(1.0f);		// initialize Matrix, Use this matrix for individual models, 
											// Es unitaria -> Solo afecta a un obj en particular
	glm::mat4 view = glm::mat4(1.0f);		// Use this matrix for ALL models -> Op grupales
	glm::mat4 projection = glm::mat4(1.0f);	// This matrix is for Projection -> Se aplica a todo el escenario.

	
	//Use "projection" in order to change how we see the information
	projection = glm::perspective(glm::radians(45.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	//projection = glm::ortho(-5.0f, 5.0f, -3.0f, 3.0f, 0.1f, 10.0f);
	
	//Use "view" in order to affect all models
	//view = glm::translate(view, glm::vec3(0.0f, 0.0f, 0.0f));
	view = glm::translate(view, glm::vec3(movX, movY, movZ));
	// pass them to the shaders
	projectionShader.setMat4("model", model);
	projectionShader.setMat4("view", view);
	// note: currently we set the projection matrix each frame, 
	//but since the projection matrix rarely changes it's often best practice to set it outside the main loop only once.
	projectionShader.setMat4("projection", projection);

	glBindVertexArray(VAO);

	projectionShader.setVec3("aColor", orange); // Tiene que ser naranja.
	glDrawArrays(GL_QUADS, 0, 24); 

	glm::vec3 outline1[] = {
		goRightUp,
		goRight,
		goRightUp,
		goRight,
		goRight,
		goRight,
		goRight,
		goRightDown,
		goRight,
		goRightDown
	};

	drawFigure(model,projectionShader,outline1,10);

	blankSpace(model,projectionShader,goRightDown);
	
	glm::vec3 outline2[] = {
		goDown,
		goDown,
		goDown,
		goDown,
		goDown,
		goLeftDown,
		goDown
	};

	drawFigure(model,projectionShader,outline2,7);

	drawNegativeFigure(model,projectionShader,outline1,10);
	
	glm::vec3 outline3[] = {
		goUp,
		goLeftUp,
		goUp,
		goUp,
		goUp,
		goUp
	};

	drawFigure(model,projectionShader,outline3,6);

	// Drawing heart
	projectionShader.setVec3("aColor", red); 
	drawLonelyCube(model,projectionShader,goRightUp);
	drawLonelyCube(model,projectionShader,goRight);
	blankSpace(model,projectionShader,goRight);
	drawNTimesCube(model,projectionShader,goRight,2);
	drawLonelyCube(model,projectionShader,goDown);
	drawNTimesCube(model,projectionShader,goLeft,4);
	drawLonelyCube(model,projectionShader,goDown);
	drawNTimesCube(model,projectionShader,goRight,4);
	drawLonelyCube(model,projectionShader,goLeftDown);
	drawNTimesCube(model,projectionShader,goLeft,2);
	drawLonelyCube(model,projectionShader,goRightDown);

	glm::vec3 seeker[] = {goRight,goRight,goRight,goRight,goUp,goUp,goUp};

	walk(model,projectionShader,seeker,7);
	
	// Draw second heart
	drawLonelyCube(model,projectionShader,goUp);

	drawLonelyCube(model,projectionShader,goRight);
	blankSpace(model,projectionShader,goRight);
	drawNTimesCube(model,projectionShader,goRight,2);
	drawLonelyCube(model,projectionShader,goDown);
	drawNTimesCube(model,projectionShader,goLeft,4);
	drawLonelyCube(model,projectionShader,goDown);
	drawNTimesCube(model,projectionShader,goRight,4);
	drawLonelyCube(model,projectionShader,goLeftDown);
	drawNTimesCube(model,projectionShader,goLeft,2);
	drawLonelyCube(model,projectionShader,goRightDown);
	
	blankSpace(model,projectionShader,goLeftDown);

	// Smile
	projectionShader.setVec3("aColor", brown); 

	drawLonelyCube(model,projectionShader,goDown);
	drawLonelyCube(model,projectionShader,goLeftDown);
	drawLonelyCube(model,projectionShader,goLeft);
	drawLonelyCube(model,projectionShader,goLeft);
	drawLonelyCube(model,projectionShader,goLeftUp);


	projectionShader.setVec3("aColor", yellow); 

	glm::vec3 background[] = {
		goLeft,
		goLeft,
		goDown,
		goRight,
		goRight,
		goDown,
		goRight,
		goRight,
		goRight,
		goRight,
		goUp,
		goRight,
		goRight,
		goUp,
		goLeft
	};

	drawFigure(model,projectionShader,background,15);
	blankSpace(model,projectionShader,goLeft);

	drawLonelyCube(model,projectionShader,goLeft);
	drawLonelyCube(model,projectionShader,goLeft);
	drawLonelyCube(model,projectionShader,goLeft);

	glm::vec3 seeker2[] = {
		goLeftUp,
		goLeft,
		goLeft,
		goLeft,
		goLeft,
	};

	walk(model,projectionShader,seeker2,5);	

	drawNTimesCube(model,projectionShader,goRight,11);

	glm::vec3 triangule[] = {
		goUp,
		goUp,
		goLeftDown
	};

	glm::vec3 trianguleInv[] = {
		goLeft,
		goLeft,
		goUp
	};

	glm::vec3 triangule2[] = {
		goLeft,
		goDown,
		goLeft
	};

	drawFigure(model,projectionShader,triangule,3);

	blankSpace(model,projectionShader,goLeft);
	drawFigure(model,projectionShader,trianguleInv,3);

	blankSpace(model,projectionShader,goLeft);
	drawFigure(model,projectionShader,triangule2,3);

	blankSpace(model,projectionShader,goLeft);
	drawFigure(model,projectionShader,trianguleInv,3);

	glm::vec3 seeker3[] = {
		goDown,
		goRight,
		goRight,
		goRight,
		goRight,
		goRight
	};

	walk(model,projectionShader,seeker3,5);

	drawLonelyCube(model,projectionShader,goRight);

	drawNTimesCube(model,projectionShader,goUp,6);
	drawLonelyCube(model,projectionShader,goRight);
	drawLonelyCube(model,projectionShader,goRight);
	drawLonelyCube(model,projectionShader,goDown);
	drawLonelyCube(model,projectionShader,goLeft);

	blankSpace(model,projectionShader,goLeft);
	drawLonelyCube(model,projectionShader,goLeft);
	drawLonelyCube(model,projectionShader,goLeft);
	drawLonelyCube(model,projectionShader,goUp);
	drawLonelyCube(model,projectionShader,goRight);

	blankSpace(model,projectionShader,goDown);
	blankSpace(model,projectionShader,goLeft);

	drawLonelyCube(model,projectionShader,goLeft);
	drawLonelyCube(model,projectionShader,goLeft);
	drawLonelyCube(model,projectionShader,goRightDown);

	blankSpace(model,projectionShader,goRight);
	blankSpace(model,projectionShader,goRight);
	blankSpace(model,projectionShader,goRight);
	blankSpace(model,projectionShader,goRight);
	blankSpace(model,projectionShader,goRight);

	drawLonelyCube(model,projectionShader,goRight);
	drawLonelyCube(model,projectionShader,goUp);
	drawLonelyCube(model,projectionShader,goRight);

	glBindVertexArray(0);

}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    /*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);*/

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
	monitors = glfwGetPrimaryMonitor();
	getResolution();

    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Practica 3", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
	glfwSetWindowPos(window, 0, 30);
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, resize);

	glewInit();


	//Mis funciones
	//Datos a utilizar
	myData();
	glEnable(GL_DEPTH_TEST);

    // render loop
    // While the windows is not closed
    while (!glfwWindowShouldClose(window))
    {
        // input
        // -----
        my_input(window);

        // render
        // Backgound color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Mi funci�n de dibujo
		display();

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void my_input(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)  //GLFW_RELEASE
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		movX -= 0.01f;
	}
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		movX += 0.01f;
	}
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		movZ += 0.01f;
	}
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		movZ -= 0.01f;
	}
	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
		movY -= 0.01f;
	}
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS) {
		movY += 0.01f;
	}

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void resize(GLFWwindow* window, int width, int height)
{
    // Set the Viewport to the size of the created window
    glViewport(0, 0, width, height);
}
