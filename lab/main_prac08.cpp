/*---------------------------------------------------------*/
/* ----------------   Pr�ctica 8 --------------------------*/
/*-----------------    2020-1   ---------------------------*/
/*------------- Alumno: Francisco Pablo Rodrigo------------*/

#define STB_IMAGE_IMPLEMENTATION
#include "esfera.h"
#include "camera.h"

Esfera my_sphere(1.0f);

void resize(GLFWwindow* window, int width, int height);
void my_input(GLFWwindow *window);
void mouse_callback(GLFWwindow *window, double xpos, double ypos);
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);

// settings
// Window size
int SCR_WIDTH = 3800;
int SCR_HEIGHT = 7600;
GLFWmonitor *monitors;
GLuint VBO, VAO, EBO;

//Camera
Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
double	lastX = 0.0f,
		lastY = 0.0f;
bool firstMouse = true;

//Timing
double	deltaTime = 0.0f,
		lastFrame = 0.0f;

//Lighting
glm::vec3 lightPos(1.2f, 1.0f, 2.0f);

void myData(void);
void display(Shader);
void getResolution(void);
void animate(void);
void LoadTextures(void);
unsigned int generateTextures(char*, bool);

//For Keyboard
float	movX = 0.0f,
		movY = 0.0f,
		movZ = -5.0f,
		rotX = 0.0f;

//Texture
//Texturas que se van a guardar en variables t_ para indicar que es una textura 
unsigned int	t_smile,
				t_toalla,
				t_unam,
				t_white,
				t_navidad,
				t_ghibli,
				t_cube;
//Ya con la variable vamos a la funcion de load texturres 

unsigned int generateTextures(const char* filename, bool alfa)

{
	unsigned int textureID;
	glGenTextures(1, &textureID);
	//Prepara recursos para una textura en dos dimensiones
	glBindTexture(GL_TEXTURE_2D, textureID);
	// set the texture wrapping parameters, cuando ocpemos el tama�o maximo se va a empezar a repetir 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	// set texture filtering parameters
	//Filtros: en caso de tener que hacer mas peque�a o mas grande , vas a ocupar un filtro lineal 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load image, create texture and generate mipmaps
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true); // tell stb_image.h to flip loaded texture's on the y-axis.
	
	//Para la transparencia 
	unsigned char *data = stbi_load(filename, &width, &height, &nrChannels, 0);
	if (data)
	{
		if (alfa)
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		else
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		return textureID;
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
		return 100;
	}

	stbi_image_free(data);
}

void getResolution()
{
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	SCR_WIDTH = mode->width;
	SCR_HEIGHT = (mode->height) - 80;

	lastX = SCR_WIDTH / 2.0f;
	lastY = SCR_HEIGHT / 2.0f;

}

void LoadTextures()
{

	t_smile = generateTextures("Texturas/awesomeface.png", 1);
	t_toalla = generateTextures("Texturas/toalla.tga", 0);
	t_unam = generateTextures("Texturas/escudo_unam.jpg", 0);
	t_white = generateTextures("Texturas/white.jpg", 0);
	t_cube = generateTextures("Texturas/Cube03.png", 0);


	//glActiveTexture(GL_TEXTURE3);
	
	// bind textures on corresponding texture units
	/*glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, t_smile);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, t_toalla);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, t_unam);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, t_white);*/
}

void myData()
{	

	float vertices[] = {
		// positions          // texture coords
		1.0f,  1.0f, 0.0f ,     0.455f, 1.0f, // top right
		1.0f, 0.0f, 0.0f ,      0.455f, 0.0f, // bottom right
		0.0f, 0.0f, 0.0f,       0.0f, 0.0f, // bottom left
		0.0f,  1.0f, 0.0f,      0.0f, 1.0f,  // top left 

		//Position				//texture coords
		- 0.5f, -0.5f, 0.5f,	0.25f, 0.0f, 	//V0 - Frontal
		0.5f, -0.5f, 0.5f,		0.5f, 0.0f, 	//V1
		0.5f, 0.5f, 0.5f,		0.5f, 0.33f, 	//V5
		-0.5f, 0.5f, 0.5f,		0.25f, 0.33f, 	//V4

		0.5f, -0.5f, -0.5f,		0.25f, 0.67f, 	//V2 - Trasera
		-0.5f, -0.5f, -0.5f,	0.5f, 0.67f, 	//V3
		-0.5f, 0.5f, -0.5f,		0.5f, 1.0f, 	//V7
		0.5f, 0.5f, -0.5f,		0.25f, 1.0f, 	//V6

		-0.5f, 0.5f, 0.5f,		0.75f, 0.33f, 	//V4 - Izq
		-0.5f, 0.5f, -0.5f,		1.0f, 0.33f, 	//V7
		-0.5f, -0.5f, -0.5f,	1.0f, 0.66f, 	//V3
		-0.5f, -0.5f, 0.5f,		0.75f, 0.66f, 	//V0

		0.5f, 0.5f, 0.5f,		0.25f, 0.33f, 	//V5 - Der
		0.5f, -0.5f, 0.5f,		0.5f, 0.33f, 	//V1
		0.5f, -0.5f, -0.5f,		0.5f, 0.66f, 	//V2
		0.5f, 0.5f, -0.5f,		0.25f, 0.66f, 	//V6

		-0.5f, 0.5f, 0.5f,		0.0f, 0.33f, 	//V4 - Sup
		0.5f, 0.5f, 0.5f,		0.25f, 0.33f, 	//V5
		0.5f, 0.5f, -0.5f,		0.25f, 0.66f, 	//V6
		-0.5f, 0.5f, -0.5f,		0.0f, 0.66f, 	//V7

		-0.5f, -0.5f, 0.5f,		0.5f, 0.33f, 	//V0 - Inf
		-0.5f, -0.5f, -0.5f,	0.75f, 0.33f, 	//V3
		0.5f, -0.5f, -0.5f,		0.75f, 0.66f, 	//V2
		0.5f, -0.5f, 0.5f,		0.5f, 0.66f, 	//V1

	};

	unsigned int indices[] = {
		0, 1, 3, // first triangle
		1, 2, 3  // second triangle
	};


	
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// texture coord attribute
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

}

void animate(void)
{

}

void display(Shader shader)
{
	//Enable Shader
	shader.use();
	// create transformations and Projection
	glm::mat4 cubo = glm::mat4(1.0f);
	glm::mat4 temp = glm::mat4(1.0f);
	glm::mat4 temp2 = glm::mat4(1.0f);
	glm::mat4 model = glm::mat4(1.0f);		// initialize Matrix, Use this matrix for individual models
	glm::mat4 view = glm::mat4(1.0f);		//Use this matrix for ALL models
	glm::mat4 projection = glm::mat4(1.0f);	//This matrix is for Projection

	//Use "projection" to include Camera
	projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
	view = camera.GetViewMatrix();

	// pass them to the shaders
	shader.setVec3("viewPos", camera.Position);
	shader.setMat4("model", model);
	shader.setMat4("view", view);
	// note: currently we set the projection matrix each frame, but since the projection matrix rarely changes it's often best practice to set it outside the main loop only once.
	shader.setMat4("projection", projection);

	glBindVertexArray(VAO);
	//Colocar c�digo aqu�
	shader.setVec3("aColor", 1.0f, 1.0f, 1.0f);
	//shader.setInt("texture1",0);
	//Tipo de textura y el indice de textura 
	//glBindTexture(GL_TEXTURE_2D, t_ghibli);
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	/*
	model = glm::translate(model, glm::vec3(0.0f, 0.0f, 1.0f));
	shader.setMat4("model", model);
	glBindTexture(GL_TEXTURE_2D, t_navidad);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);*/
	

	//Posicion del cubo
	shader.setVec3("aColor", 1.0f, 1.0f, 1.0f);
	model = glm::translate(model, glm::vec3(4.0f,0.0f, 0.0f));
	glBindTexture(GL_TEXTURE_2D, t_cube);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	shader.setMat4("model", model);
	glDrawArrays(GL_QUADS, 4, 28);


	glBindVertexArray(0);

}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    /*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);*/

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
	monitors = glfwGetPrimaryMonitor();
	getResolution();

    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Practica 8", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
	glfwSetWindowPos(window, 0, 30);
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, resize);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	//To Enable capture of our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

	glewInit();


	//Mis funciones
	//Datos a utilizar
	LoadTextures();
	myData();
	my_sphere.init();
	glEnable(GL_DEPTH_TEST);

	Shader projectionShader("shaders/shader_texture_color.vs", "shaders/shader_texture_color.fs");

    // render loop
    // While the windows is not closed
    while (!glfwWindowShouldClose(window))
    {
		// per-frame time logic
		// --------------------
		double currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
        // input
        // -----
        my_input(window);
		animate();

        // render
        // Backgound color
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Mi funci�n de dibujo
		display(projectionShader);

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);

    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void my_input(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboard(FORWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboard(BACKWARD, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboard(LEFT, (float)deltaTime);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboard(RIGHT, (float)deltaTime);
	

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void resize(GLFWwindow* window, int width, int height)
{
    // Set the Viewport to the size of the created window
    glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}

	double xoffset = xpos - lastX;
	double yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}