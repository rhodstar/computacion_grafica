/*---------------------------------------------------------*/
/* ----------------   Práctica 2 --------------------------*/
/*-----------------    2020-1   ---------------------------*/
/*------------- Rodrigo Francisco Pablo  ------------------*/
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>

void resize(GLFWwindow* window, int width, int height);
void my_input(GLFWwindow *window);

// settings
// Window size
int SCR_WIDTH = 800;
int SCR_HEIGHT = 600;

GLFWmonitor *monitors;
GLuint VBO, VAO, EBO;
GLuint shaderProgramRed, shaderProgramColor;

static const char* myVertexShader = "										\n\
#version 330 core															\n\
																			\n\
layout (location = 0) in vec4 aPos;											\n\
																			\n\
void main()																	\n\
{																			\n\
    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);						\n\
}";

static const char* myVertexShaderColor = "									\n\
#version 330 core															\n\
																			\n\
layout (location = 0) in vec3 aPos;											\n\
layout (location = 1) in vec3 aColor;										\n\
out vec3 ourColor;															\n\
void main()																	\n\
{																			\n\
    gl_Position = vec4(aPos, 1.0);											\n\
	ourColor = aColor;														\n\
}";

// Fragment Shader
static const char* myFragmentShaderRed = "									\n\
#version 330																\n\
																			\n\
out vec3 finalColor;														\n\
																			\n\
void main()																	\n\
{																			\n\
    finalColor = vec3(1.0f, 0.0f, 0.0f);									\n\
}";

static const char* myFragmentShaderColor = "								\n\
#version 330 core															\n\
out vec4 FragColor;															\n\
in vec3 ourColor;															\n\
																			\n\
void main()																	\n\
{																			\n\
	FragColor = vec4(ourColor, 1.0f);										\n\
}";

void myData(void);
void setupShaders(void);
void display(void);
void getResolution(void);


void getResolution()
{
	const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	SCR_WIDTH = mode->width;
	SCR_HEIGHT = (mode->height) - 80;
}

void myData()
{
	

	float vertices[] = 
	{
		// positions         //color
		-0.7f,  0.8f, 0.0f,  1.0f, 0.0f, 0.0f, // A.0 --C
		-0.4f,  0.8f, 0.0f,  1.0f, 0.0f, 0.0f, // B.1 --C
		-0.4f,  0.7f, 0.0f,  1.0f, 0.0f, 0.0f, // C.2 --C
		-0.6f,  0.7f, 0.0f,  1.0f, 0.0f, 0.0f, // D.3 --C
		-0.6f,  0.3f, 0.0f,  1.0f, 0.0f, 0.0f, // E.4 --C
		-0.4f,  0.3f, 0.0f,  1.0f, 0.0f, 0.0f, // F.5 --C
		-0.4f,  0.2f, 0.0f,  1.0f, 0.0f, 0.0f, // G.6 --C
		-0.7f,  0.2f, 0.0f,  1.0f, 0.0f, 0.0f, //  H.7 --C

		-0.2f,  0.8f, 0.0f,  0.0f, 1.0f, 0.0f, //  I.8 --R
		0.1f,  0.8f, 0.0f,  0.0f, 1.0f, 0.0f, //  J.9 --R
		0.1f,  0.5f, 0.0f,  0.0f, 1.0f, 0.0f, //  K.10 --R
		0.2f,  0.2f, 0.0f,  0.0f, 1.0f, 0.0f, //  L.11 --R
		0.1f,  0.2f, 0.0f,  0.0f, 1.0f, 0.0f, //  M.12 --R
		-0.1f,  0.4f, 0.0f,  0.0f, 1.0f, 0.0f, //  N.13 --R
		-0.1f,  0.2f, 0.0f,  0.0f, 1.0f, 0.0f, //  O.14 --R
		-0.2f,  0.2f, 0.0f,  0.0f, 1.0f, 0.0f, //  P.15 --R
		-0.1f,  0.7f, 0.0f,  0.0f, 1.0f, 0.0f, //  Q.16 --R
		0.0f,  0.7f, 0.0f,  0.0f, 1.0f, 0.0f, //  R.17 --R
		0.0f,  0.6f, 0.0f,  0.0f, 1.0f, 0.0f, //  S.18 --R
		-0.1f,  0.6f, 0.0f,  0.0f, 1.0f, 0.0f, //  T.19 --R
		-0.1f,  0.5f, 0.0f,  0.0f, 1.0f, 0.0f, //  U.20 --R

		0.3f,  0.8f, 0.0f,  1.0f, 0.0f, 1.0f, //  V.21 --F
		0.6f,  0.8f, 0.0f,  1.0f, 0.0f, 1.0f, //  W.22 --F
		0.6f,  0.7f, 0.0f,  1.0f, 0.0f, 1.0f, //  X.23 --F
		0.4f,  0.7f, 0.0f,  1.0f, 0.0f, 1.0f, //  Y.24 --F
		0.4f,  0.5f, 0.0f,  1.0f, 0.0f, 1.0f, //  Z.25 --F
		0.6f,  0.5f, 0.0f,  1.0f, 0.0f, 1.0f, //  0.26 --F
		0.6f,  0.4f, 0.0f,  1.0f, 0.0f, 1.0f, //  1.27 --F
		0.4f,  0.4f, 0.0f,  1.0f, 0.0f, 1.0f, //  2.28 --F
		0.4f,  0.2f, 0.0f,  1.0f, 0.0f, 1.0f, //  3.29 --F
		0.3f,  0.2f, 0.0f,  1.0f, 0.0f, 1.0f, //  4.30 --F

		0.0f,  0.0f, 0.0f,  1.0f, 0.0f, 1.0f, //  5.31 --STAR
		0.1f,  -0.2f, 0.0f,  0.0f, 1.0f, 0.0f, //  6.32 --STAR
		0.3f,  -0.2f, 0.0f,  0.0f, 0.0f, 1.0f, //  7.33 --STAR
		0.15f,  -0.4f, 0.0f,  0.5f, 0.0f, 0.5f, //  8.34 --STAR
		0.2f,  -0.6f, 0.0f,  0.0f, 0.5f, 0.0f, //  9.35 --STAR
		0.0f,  -0.5f, 0.0f,  0.5f, 0.9f, 0.0f, //  10.36 --STAR
		-0.2f,  -0.6f, 0.0f,  0.0f, 0.5f, 0.0f, //  11.37 --STAR
		-0.15f,  -0.4f, 0.0f,  0.4f, 0.0f, 0.3f, //  12.38 --STAR
		-0.3f,  -0.2f, 0.0f,  1.0f, 0.5f, 0.2f, //  13.39 --STAR
		-0.1f,  -0.2f, 0.0f,  0.5f, 0.5f, 0.5f, //  14.40 --STAR
		
		0.0f,  -0.3f, 0.0f,  1.0f, 0.0f, 0.0f, //  15.41 --STAR

	};

	// Es un arreglo que le indica a nuestro programa en ue orden tomar los vertices
	unsigned int indices[] =
	{
		0,1,2,3, //--C.1
		0,3,4,7, //--C.2
		4,5,6,7, //--C.3

		8,9,17,16, //--R.4
		9,10,18,17, //--R.5
		18,10,20,19, //--R.6
		20,11,12,13, //--R.7
		8,16,14,15, //--R.8

		21,22,23,24, //--F.9
		25,26,27,28, //--F.10
		21,24,29,30, //--F.11

		31,32,41,40, //--STAR.12
		33,34,41,32, //--STAR.13
		34,35,36,41, //--STAR.14
		36,37,38,41, //--STAR.15
		38,39,40,41, //--STAR.16

	};

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	glGenBuffers(1, &VBO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// position attribute
	//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	//Para trabajar con indices (Element Buffer Object)
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

}

void setupShaders()
{
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &myVertexShader, NULL);
	glCompileShader(vertexShader);

	unsigned int vertexShaderColor = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderColor, 1, &myVertexShaderColor, NULL);
	glCompileShader(vertexShaderColor);

	unsigned int fragmentShaderRed = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderRed, 1, &myFragmentShaderRed, NULL);
	glCompileShader(fragmentShaderRed);

	unsigned int fragmentShaderColor = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderColor, 1, &myFragmentShaderColor, NULL);
	glCompileShader(fragmentShaderColor);


	//Crear el Programa que combina Geometr�a con Color
	shaderProgramRed = glCreateProgram();
	glAttachShader(shaderProgramRed, vertexShader);
	glAttachShader(shaderProgramRed, fragmentShaderRed);
	glLinkProgram(shaderProgramRed);

	shaderProgramColor = glCreateProgram();
	glAttachShader(shaderProgramColor, vertexShaderColor);
	glAttachShader(shaderProgramColor, fragmentShaderColor);
	glLinkProgram(shaderProgramColor);
	//Check for errors 

	//ya con el Programa, el Shader no es necesario
	glDeleteShader(vertexShader);
	glDeleteShader(vertexShaderColor);
	glDeleteShader(fragmentShaderRed);
	glDeleteShader(fragmentShaderColor);

}

void display(void)
{
	glUseProgram(shaderProgramColor);

	glBindVertexArray(VAO);
	//glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);

	glPointSize(5.0);
	// va a los indices para que sepa como utilizar los vertices
	glDrawElements(GL_QUADS, 64, GL_UNSIGNED_INT, 0); 

	glBindVertexArray(0);

	glUseProgram(0);

}

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    /*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);*/

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
	monitors = glfwGetPrimaryMonitor();
	getResolution();

    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "Practica 2", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
	glfwSetWindowPos(window, 0, 30);
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, resize);

	glewInit();


	//Mis funciones
	//Datos a utilizar
	myData();
	//Configurar Shaders
	setupShaders();
    

    // render loop
    // While the windows is not closed
    while (!glfwWindowShouldClose(window))
    {
        // input
        // -----
        my_input(window);

        // render
        // Backgound color
        glClearColor(0.09f, 0.9f, 0.8f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

		//Mi funci�n de dibujo
		display();

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void my_input(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)  //GLFW_RELEASE
        glfwSetWindowShouldClose(window, true);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void resize(GLFWwindow* window, int width, int height)
{
    // Set the Viewport to the size of the created window
    glViewport(0, 0, width, height);
}
