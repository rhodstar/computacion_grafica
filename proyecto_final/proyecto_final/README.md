# Laboratorio Computación gráfica e interacción humano-computadora

```shell
OS: Manjaro
Editor: Visual Studio Code
Compilador: g++
septiembre de 2019
```

## Instalación

Para utilizar OpenGl en Linux, particularmente en ArchLinux se deben de instalar lo siguiente

```shell
sudo pacman -S mesa glew glfw glm 
```

Para comprobar la instalación

```shell
glxinfo  |grep -i version
```

**NOTA: No todas las computadoras tienen tarjeta gráfica y esto límita las herramientas de OpenGl que podemos usar.**

## Compilación

Para compilar los programas de OpenGl se utliza g++

```shell
g++ -lGL -LGLEW -lglfw proyecto.c++
```

